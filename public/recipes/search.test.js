import {buildRecipesWithIngredientNameUrl} from "./search.js";

QUnit.module('buildRecipesWithIngredientNameUrl', function() {
    QUnit.test('qunit', function(assert) {
        assert.equal(1,1);
    });
    QUnit.test('without ingredient name', function(assert) {
        const expectedUrl = "/recipes"
        // "/recipes"
        const ingredientNames = [];
        assert.equal(buildRecipesWithIngredientNameUrl(ingredientNames), expectedUrl);
    });
    QUnit.test('with one ingredient name', function(assert) {
        const ingredientNames = ["patate"];
        // "/recipes?withIngredient=patate"
        const expectedUrl = "/recipes?withIngredient=patate"
        assert.equal(buildRecipesWithIngredientNameUrl(ingredientNames), expectedUrl);
    });
    QUnit.test('with many ingredient name', function(assert) {
        // "/recipes?withIngredient=patate&withIngredient=riz"

        const ingredientNames = ["patate", "riz"];
        const expectedUrl = "/recipes?withIngredient=patate&withIngredient=riz"
        assert.equal(buildRecipesWithIngredientNameUrl(ingredientNames), expectedUrl);
    });
    QUnit.test('with many ingredient name', function(assert) {
        // "/recipes?withIngredient=patate&withIngredient=riz"

        const ingredientNames = ["oeuf", "riz", "jambon"];
        const expectedUrl = "/recipes?withIngredient=oeuf&withIngredient=riz&withIngredient=jambon"
        assert.equal(buildRecipesWithIngredientNameUrl(ingredientNames), expectedUrl);
    });
    QUnit.test('whitespace in ingredient name', function(assert) {

        const ingredientNames = ["petits pois"];
        const expectedUrl = "/recipes?withIngredient=petits pois"
        assert.equal(buildRecipesWithIngredientNameUrl(ingredientNames), expectedUrl);
    });

    // MDN

    // const tableau = ["a","bb","ccc"]
    // tableau.map(ingredient => ingredient.length)
    // [1, 2, 3]
    // Equivalent à tableau.stream().map(...)

    // https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/join
    // tableau.join("|")
    // "a|bb|ccc"

});