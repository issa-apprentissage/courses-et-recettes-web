import {config} from "../config.js";

/**
 * @param {string[]} ingredientNames
 * @returns {string}
 */
function buildRecipesWithIngredientNameUrl(ingredientNames) {

    const formattedIngredients = ingredientNames   // ['patate', 'riz', 'huile']
        .map(name => `withIngredient=${name}`)     // ['withIngredient=patate', 'withIngredient=riz' , ]
        .join('&');                                // 'withIngredient=patate&withIngredient=riz'

    if (ingredientNames.length === 0) {
        return `/recipes`;
    } else {
        return `/recipes?${formattedIngredients}`;
    }
}

/**
 * @param {string[]} ingredientNames
 * @example ["patate", "huile"]
 * @return {Promise<RecipesSearchResults>}
 */
const searchRecipesByIngredientName = async (ingredientNames) => {

    let url = buildRecipesWithIngredientNameUrl(ingredientNames);
    return fetch(`${config.API_URL}${url}`)
        .then(response => response.json());
}


/**
 * @param {string} elementId
 * @param {RecipesSearchResults} recipesSearchResults
 */
const renderRecipesSearchResults = (elementId, recipesSearchResults) => {
    document.getElementById(elementId).innerHTML = recipesSearchResultsToHtml(recipesSearchResults)
};

/**
 *
 * @param {RecipesSearchResults} recipesSearchResults
 * @return {string}
 */
function recipesSearchResultsToHtml(recipesSearchResults) {
    const listItems = recipesSearchResults
        .map(value => `<li>${value}</li>`)
        .join("\n")
    return `
<ul>
    ${listItems}
</ul>`;
}

/**
 * @param elementId
 */
const clearRecipesSearchResults = (elementId) => {
    document.getElementById(elementId).innerHTML = ""
};

/**
 * @typedef RecipesSearchResults
 * @type string[]
 */

export {
    clearRecipesSearchResults,
    renderRecipesSearchResults,
    searchRecipesByIngredientName,
    buildRecipesWithIngredientNameUrl
}