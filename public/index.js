import {
    clearRecipesSearchResults,
    renderRecipesSearchResults,
    searchRecipesByIngredientName
} from "/recipes/search.js";

const onSearchRecipesByIngredientName = (event) => {

    /**
     * @type {string}
     * @example
     * "patate,huile"
     **/
    const ingredientName = document.getElementById("ingredientName").value;

    /**
     * @type {string[]}
     * @example
     * ["patate", "huile"]
     **/
    const ingredientNames = ingredientName.split(",");

    searchRecipesByIngredientName(ingredientNames)
        .then(results => renderRecipesSearchResults("recipes-search-results", results))
        .catch(() => clearRecipesSearchResults("recipes-search-results"));
    event.preventDefault()
}

document.getElementById("recipe-by-ingredient-search")
    .addEventListener("submit", onSearchRecipesByIngredientName)